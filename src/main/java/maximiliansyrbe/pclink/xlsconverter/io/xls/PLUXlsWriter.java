package maximiliansyrbe.pclink.xlsconverter.io.xls;

import jxl.Workbook;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.Pattern;
import jxl.format.*;
import jxl.write.Number;
import jxl.write.*;
import maximiliansyrbe.pclink.xlsconverter.model.PLU;
import maximiliansyrbe.pclink.xlsconverter.model.Record;

import java.io.File;
import java.io.IOException;

public class PLUXlsWriter {

    public static void writePlu2Excel(final PLU plu, final String filename) throws IOException, WriteException {
        WritableWorkbook wb = null;

        try {
            wb = Workbook.createWorkbook(new File(filename));
            WritableSheet excelSheet = wb.createSheet("Artikelliste", 0);

            addHeader(excelSheet);

            for (int i = 0; i < plu.getRecords().size(); i++) {
                addRow(plu, excelSheet, i);
            }
            wb.write();
        } finally {
            if (wb != null) {
                wb.close();
            }
        }
    }

    private static void addHeader(final WritableSheet excelSheet) throws WriteException {
        WritableFont headerFont =
                new WritableFont(WritableFont.ARIAL, 12, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.WHITE);
        WritableCellFormat format = new WritableCellFormat(headerFont);
        format.setBackground(Colour.SEA_GREEN);
        format.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.AUTOMATIC);

        Label label = new Label(0, 0, "PLU", format);
        excelSheet.addCell(label);

        label = new Label(1, 0, "Name", format);
        excelSheet.addCell(label);

        label = new Label(2, 0, "Preis (Cent)", format);
        excelSheet.addCell(label);

        label = new Label(3, 0, "Dept.", format);
        excelSheet.addCell(label);
    }

    private static void addRow(final PLU p, final WritableSheet sheet, final int i) throws WriteException {
        Record record = p.getRecords().get(i);
        int row = i + 1;

        WritableFont font = new WritableFont(WritableFont.ARIAL, 11);
        WritableCellFormat f = new WritableCellFormat(font);
        f.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.AUTOMATIC);

        if (row % 2 == 0) {
            f.setBackground(Colour.LIGHT_GREEN, Pattern.SOLID);
        } else {
            f.setBackground(Colour.WHITE);
        }

        sheet.addCell(new Number(0, row, Double.parseDouble(record.getPluCode()), f));
        sheet.addCell(new Label(1, row, record.getField4().getText(), f));
        sheet.addCell(new Number(2, row, Double.parseDouble(record.getField3().getPrice()), f));
        sheet.addCell(new Number(3, row, record.getField1().getDepartment(), f));
    }
}
