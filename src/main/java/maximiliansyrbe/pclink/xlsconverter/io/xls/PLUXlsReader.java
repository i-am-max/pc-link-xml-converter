package maximiliansyrbe.pclink.xlsconverter.io.xls;

import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.read.biff.BiffException;
import maximiliansyrbe.pclink.xlsconverter.model.Record;
import maximiliansyrbe.pclink.xlsconverter.model.*;
import maximiliansyrbe.pclink.xlsconverter.utils.StringFormatter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

public class PLUXlsReader {

    public static PLU readFromFile(final String filename) throws IOException, BiffException {
        Workbook workbook = null;

        try {
            WorkbookSettings settings = new WorkbookSettings();
            settings.setEncoding("Cp1252");

            workbook = Workbook.getWorkbook(new File(filename), settings);
            final Sheet sheet = workbook.getSheet(0);
            final PLU plu = new PLU();
            final List<Record> records = new ArrayList<>();

            plu.setRecords(records);

            for (int i = 1; i <= 200; i++) {
                addRecord(sheet, plu, i);
            }

            return plu;
        } finally {
            if (workbook != null) {
                workbook.close();
            }
        }
    }

    private static void addRecord(final Sheet sheet, final PLU plu, final int row) {
        final String pluCode = getCellContent(sheet, row, 0, Integer.toString(row));
        final String text = getCellContent(sheet, row, 1, "");
        final String price = getCellContent(sheet, row, 2, "");
        final String dept = getCellContent(sheet, row, 3, "1");

        plu.getRecords().add(
                new Record(0,
                        new Field1(StringFormatter.formatDepartment(dept), new Program("Preset")),
                        new Field3(price),
                        new Field4(StringFormatter.formatItemText(text)),
                        StringFormatter.formatPluCodeLong(pluCode)));
    }

    private static String getCellContent(final Sheet sheet, final int row, final int column, final String defaultValue) {
        return Optional.of(sheet.getCell(column, row).getContents())
                .filter(Predicate.not(String::isEmpty)).orElse(defaultValue);

    }
}
