package maximiliansyrbe.pclink.xlsconverter;

import lombok.RequiredArgsConstructor;
import maximiliansyrbe.pclink.xlsconverter.service.PriceListConverterService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;

@SpringBootApplication
@ConfigurationPropertiesScan
@RequiredArgsConstructor
public class PCLinkConverterApplication implements CommandLineRunner {

    private final PriceListConverterService service;
    private final Environment env;

    public static void main(String[] args) {
        SpringApplication.run(PCLinkConverterApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        if (!env.acceptsProfiles(Profiles.of("test"))) {
            service.convertPriceList2Xml();
        }
    }
}
