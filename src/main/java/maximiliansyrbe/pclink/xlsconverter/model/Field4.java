package maximiliansyrbe.pclink.xlsconverter.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.*;

import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Field4 {
    @JacksonXmlProperty(localName = "TEXT")
    @Size(min = 12, max = 12)
    private String text;

    @Override
    public String toString()
    {
        return "ClassPojo [TEXT = "+ text +"]";
    }
}
