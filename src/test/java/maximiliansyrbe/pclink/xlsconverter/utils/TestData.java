package maximiliansyrbe.pclink.xlsconverter.utils;

public class TestData {
    public static final String PLU_XML_IN = "PLU.xml";
    public static final String PLU_XML_OUT = "PLU_out.xml";
    public static final String PRICELIST_XLS_OUT = "Pricelist.xls";
}
