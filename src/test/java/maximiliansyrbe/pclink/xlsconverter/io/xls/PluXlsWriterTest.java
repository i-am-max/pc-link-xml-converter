package maximiliansyrbe.pclink.xlsconverter.io.xls;

import jxl.write.WriteException;
import maximiliansyrbe.pclink.xlsconverter.utils.TestData;
import maximiliansyrbe.pclink.xlsconverter.io.xml.PLUXmlReader;
import maximiliansyrbe.pclink.xlsconverter.model.PLU;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

import java.io.File;
import java.io.IOException;

class PluXlsWriterTest {

    @AfterEach
    void cleanUp() {
        new File(TestData.PRICELIST_XLS_OUT).delete();
    }

    @Test
    void writePlu2Excel() throws IOException, WriteException {
        PLU plu = PLUXmlReader.readFromFile(TestData.PLU_XML_IN);
        PLUXlsWriter.writePlu2Excel(plu, TestData.PRICELIST_XLS_OUT);

        Assert.isTrue(new File(TestData.PRICELIST_XLS_OUT).exists(), "XLS should exist");
    }
}
